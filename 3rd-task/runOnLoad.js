function runOnLoad(fn) {
  if (document.readyState === 'loading') {
    window.addEventListener('load', fn);
  } else {
    fn();
  }
}
